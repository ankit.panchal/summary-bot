require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const { Configuration, OpenAIApi } = require('openai');
const axios = require('axios');

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

const app = express();

app.use(bodyParser.json());

app.post('/webhook', async (req, res) => {
  const gitlabEvent = req.header('X-Gitlab-Event');

  if (gitlabEvent === 'Issue Hook') {
    const issue = req.body.object_attributes;
    const project_id = req.body.project_id;

    console.log('issue', issue);
    if (issue.action === 'open') {
      const summary = await getSummary(issue.description);
      await postComment(project_id, issue.iid, summary);
    }
  }

  res.status(200).send('OK');
});

const postComment = async (project_id, issue_iid, comment) => {
  const gitlabApiUrl = `http://localhost:3000/api/v4/projects/3/issues/${issue_iid}/notes`;

  const accessToken = process.env.GITLAB_ACCESS_TOKEN;

  try {
    await axios.post(
      gitlabApiUrl,
      { body: comment },
      { headers: { 'PRIVATE-TOKEN': accessToken } }
    );
    console.log('Comment posted successfully');
  } catch (error) {
    console.error(`Error posting comment: ${error}`);
  }
};

const getSummary = async (text) => {
  const prompt = `Summarize the following issue description:\n\n${text}\n\nSummary:`;

  try {
    const response = await openai.createCompletion({
      model: 'text-davinci-003',
      prompt,
      temperature: 0.7,
      max_tokens: 1000,
      top_p: 1.0,
      frequency_penalty: 0.0,
      presence_penalty: 1,
    });

    return response.data.choices[0].text.trim();
  } catch (error) {
    console.error(`Error generating summary: ${error}`);
    return null;
  }
};

const port = process.env.PORT || 3010;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
